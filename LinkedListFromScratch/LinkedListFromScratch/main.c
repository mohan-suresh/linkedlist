// Create a linked list from scratch with insert at end - Added
// Feature 1: Insert at beginning - DONE - DEPRECIATED
// Feature 2: Insert function with begining, end, and position - DONE

#include<stdio.h>
#include<stdlib.h>

// Structure for the node 
typedef struct mynode {
	struct mynode* ptr;
	int val;
} mynode_t;

// Enum to find the insertion type
typedef enum Position {
	BEGINNING,
	END,
	INBETWEEN
} inserttype_t;

// Globally store the value of head pointer (pointer to node)
struct mynode* head = NULL;

mynode_t* CreateNode(int val);
//void PushListAtEnd(mynode_t*);
void InsertInList(mynode_t*, inserttype_t type, int pos);
void PrintAllNodes();

// Created a new node in heap and sets the value to it. Returns pointer that points to the node.
mynode_t* CreateNode(int value) {
	mynode_t* newNodePtr = (struct mynode*)malloc(sizeof(mynode_t));

	if (newNodePtr != NULL) {
		(*newNodePtr).ptr = NULL;
		(*newNodePtr).val = value;
	}
	return newNodePtr; 
}

void InsertInList(mynode_t* pnode_to_insert, inserttype_t type, int pos)
{
	mynode_t* nodePtr = head;
	mynode_t* nodePtr2;
	if (head == NULL) { // List is EMPTY
		head = pnode_to_insert;
		return;
	}
	if (type == END) {
		while (nodePtr->ptr != NULL)
			nodePtr = nodePtr->ptr;
		nodePtr->ptr = pnode_to_insert;
	}
	else if (( type == BEGINNING ) || ( type == INBETWEEN && pos == 1 ) ) {
		head = pnode_to_insert;
		(*pnode_to_insert).ptr = nodePtr; //nodePtr has backup address of head of list before insertion
	}
	else { // inserting in between and not after head
		if (pos == 0) {
			printf("\n invalid - cant insert at 0\n");
			return;
		}
		pos--;
		while (pos-- && nodePtr->ptr != NULL) {
			if (pos == 0) {
				// now if position gets to 0, then it means to insert the node after (*nodePtr) node
				nodePtr2 = nodePtr->ptr;         // backup the address of the node that is after (*nodePtr) node 
				nodePtr->ptr = pnode_to_insert;  // insert the new node after (*nodePtr) node
				pnode_to_insert->ptr = nodePtr2; // Connect the remaining nodes that were present previously after (*nodePtr) node
				break;
			}
			else { 
				// Continue iterating linearly
				nodePtr = nodePtr->ptr;
			}
		}
		if (pos == 0)
			nodePtr->ptr = pnode_to_insert;
		else
			printf("Sorry, Cant insert at that position,\nAs the position is larger than current list\n\n");
	}
}

// Function that prints all the nodes in the linked list. 
void PrintAllNodes() {
	mynode_t* nodePtr = head;
	while (nodePtr != NULL) {
		printf("%d ", nodePtr->val);
		nodePtr = nodePtr->ptr;
	}
}

void main() {

	printf("Trying to Create a Linked List from Scratch...\n");
	mynode_t* nodePtr;
	printf("Enter 5 numbers to insert into the line--\n");
	
	// Test Inputs
	for (int i = 0; i < 15; i++) {
		nodePtr = CreateNode(i);
		InsertInList(nodePtr, END, 0);
	}

	// Test Inputs
	for (int i = 111; i < 444; i= i+111) {
		nodePtr = CreateNode(i); 
		InsertInList(nodePtr, INBETWEEN, 17);
	}

	PrintAllNodes();
}
