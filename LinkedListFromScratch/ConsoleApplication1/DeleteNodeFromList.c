// Delete a Node from a List via Value.
#include<stdio.h>

struct Node {
    int data;
    struct Node* next;
};

struct Node* head;
struct Node* CreateNode(int value) {
    struct Node* aNodePtr;
    aNodePtr = (struct Node*)malloc(sizeof(struct Node));
    aNodePtr->data = value;
    aNodePtr->next = NULL;
    return aNodePtr;
}

void InsertAtEnd(struct Node* newNodePtr) {
    //printf("At Function InsertAtEnd(). Going to insert Node with data %d\n", newNodePtr->data);
    struct Node* nodePtr = head;
    if (head == NULL) {
        head = newNodePtr;
        return;
    }
    while (nodePtr->next != NULL) {
        nodePtr = nodePtr->next;
    }
    nodePtr->next = newNodePtr;
}
void PrintAllNodes() {
    printf("In PrintAllNodes() \n");
    struct Node* nodePtr = head;
    while (nodePtr != NULL) {
      //  printf("In Loop");
        printf("Val = %d ", nodePtr->data);
        nodePtr = nodePtr->next;
    }
}
void DeleteWithVal(int val) {
    struct Node* myPtr1 = head; //myPtr1 always holds the address of the previous node of the target, unless the first node is deleted. 
    struct Node* myPtr2 = NULL; //myPtr2 holds the address of the Target Node, if case if the count of nodes is more than one
    if (myPtr1 != NULL) {
        myPtr2 = myPtr1->next;
    }
    else
    {
        return; // head is empty
    }
    // Deleting the first Node
    if (myPtr1->data == val) {
        printf("Deleting First Node\n");
        head = myPtr1->next;
        free(myPtr1);
        return;
    }

    while (myPtr2 != NULL) {
        printf("LoopCtr and myptr2 tracker . myptr2->data = %d. \n",myPtr2->data);
        if (myPtr2->data == val) {

            myPtr1->next = myPtr2->next;
            free(myPtr2);
            return;
        }
        myPtr1 = myPtr1->next;
        myPtr2 = myPtr2->next;
    }
}
int main() {
    head = NULL;
    int ctr = 0;
    struct Node* newNodePtr;
    for (ctr = 15; ctr < 20; ctr++) {
        newNodePtr = CreateNode(ctr);
        InsertAtEnd(newNodePtr);
    }
    PrintAllNodes();
    DeleteWithVal(15);
    PrintAllNodes();
}
